package org.gb.carrepairhistorytwoapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BrokenPart {
    LOOF_PANEL("프론트 판넬"),
    TRUNK("본네트"),
    REAR_PANEL("루프판넬"),
    TRUNKS("트렁크"),
    REAR_PANELS("리어판넬"),
    FRONT_WHEELHOUSE("앞휠하우스"),
    FILLERS("필러"),
    FRONT_DOOR("앞문"),
    BACK_DOOR("뒷문"),
    BACK_FENDER("뒤펜더"),
    REAR_WHEELHOUSE("뒤휠하우스");

    public final String name;
}
