package org.gb.carrepairhistorytwoapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CarName {
    AVANTE("아반떼"),
    FORTE("포르테"),
    SONATA("소나타"),
    K3("k-3"),
    K5("k-5"),
    K7("k-7"),
    GRANGER("그렌저"),
    GENESIS("제네시스");

    private final String name;
}
