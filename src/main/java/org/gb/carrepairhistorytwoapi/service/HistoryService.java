package org.gb.carrepairhistorytwoapi.service;

import lombok.RequiredArgsConstructor;
import org.gb.carrepairhistorytwoapi.entity.Car;
import org.gb.carrepairhistorytwoapi.entity.History;
import org.gb.carrepairhistorytwoapi.model.history.HistoryCreateRequest;
import org.gb.carrepairhistorytwoapi.model.history.HistoryDateChangeRequest;
import org.gb.carrepairhistorytwoapi.model.history.HistoryItem;
import org.gb.carrepairhistorytwoapi.model.history.HistoryResponse;
import org.gb.carrepairhistorytwoapi.repository.HistoryRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HistoryService {
    private final HistoryRepository historyRepository;

    public History getHistoryData(long id) {
        return historyRepository.findById(id).orElseThrow();
    }

    public void setHistory(Car car, HistoryCreateRequest request) {
        History addData = new History();
        addData.setCar(car);
        addData.setBrokenPart(request.getBrokenPart());
        addData.setRepairMemo(request.getRepairMemo());
        addData.setIsSelf(request.getIsSelf());
        addData.setDateProblem(LocalDate.now());
        historyRepository.save(addData);
    }

    public List<HistoryItem> getHistorys() {
        List<History> originList = historyRepository.findAll();
        List<HistoryItem> result = new LinkedList<>();

        for (History history: originList) {
            HistoryItem addItem = new HistoryItem();
            addItem.setCarCarName(history.getCar().getCarName().getName());
            addItem.setId(history.getId());
            addItem.setBrokenPart(history.getBrokenPart().getName());
            addItem.setIsSelf(history.getIsSelf() ? "예" : "아니오");
            result.add(addItem);
        }
        return result;
    }

    public HistoryResponse getHistory(long id) {
        History originData = historyRepository.findById(id).orElseThrow();
        HistoryResponse response = new HistoryResponse();

        response.setCarCarName(originData.getCar().getCarName().getName());
        response.setId(originData.getId());
        response.setBrokenPart(originData.getBrokenPart().getName());
        response.setIsSelf(originData.getIsSelf() ? "예" : "아니오");
        response.setDateProblem(originData.getDateProblem());
        response.setRepairMemo(originData.getRepairMemo());
        return response;
    }

    public void putDateChange(long id, HistoryDateChangeRequest request) {
        History originData = historyRepository.findById(id).orElseThrow();

        originData.setDateProblem(request.getDateProblem());

        historyRepository.save(originData);
    }
}
