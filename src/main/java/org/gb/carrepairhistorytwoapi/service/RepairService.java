package org.gb.carrepairhistorytwoapi.service;

import lombok.RequiredArgsConstructor;
import org.gb.carrepairhistorytwoapi.entity.History;
import org.gb.carrepairhistorytwoapi.entity.Repair;
import org.gb.carrepairhistorytwoapi.model.repair.RepairCostChangeRequest;
import org.gb.carrepairhistorytwoapi.model.repair.RepairCreateRequest;
import org.gb.carrepairhistorytwoapi.model.repair.RepairItem;
import org.gb.carrepairhistorytwoapi.model.repair.RepairResponse;
import org.gb.carrepairhistorytwoapi.repository.RepairRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RepairService {
    private final RepairRepository repairRepository;

    public void setRepair(History history, RepairCreateRequest request) {
        Repair addData = new Repair();

        addData.setHistory(history);
        addData.setRepairShop(request.getRepairShop());
        addData.setDateRepair(LocalDate.now());
        addData.setRepairCost(request.getRepairCost());
        addData.setIsCompleted(request.getIsCompleted());

        repairRepository.save(addData);
    }

    public List<RepairItem> getRepairs() {
        List<Repair> originList = repairRepository.findAll();
        List<RepairItem> result = new LinkedList<>();

        for (Repair repair : originList) {
            RepairItem addItem = new RepairItem();
            addItem.setHistoryCarCarName(repair.getHistory().getCar().getCarName().getName());
            addItem.setHistoryBrokenPart(repair.getHistory().getBrokenPart().getName());
            addItem.setHistoryIsSelf(repair.getHistory().getIsSelf() ? "직접수리" : "카센타수리");
            addItem.setRepairShop(repair.getRepairShop());
            addItem.setRepairCost(repair.getRepairCost());
            addItem.setIsCompleted(repair.getIsCompleted() ? "수리완료" : "수리중");
            result.add(addItem);
        }
        return result;
    }

    public RepairResponse getRepair(long id) {
        Repair originData = repairRepository.findById(id).orElseThrow();
        RepairResponse response = new RepairResponse();

        response.setHistoryCarCarName(originData.getHistory().getCar().getCarName().getName());
        response.setHistoryBrokenPart(originData.getHistory().getBrokenPart().getName());
        response.setHistoryDateProblem(originData.getHistory().getDateProblem());
        response.setHistoryIsSelf(originData.getHistory().getIsSelf() ? "직접수리" : "카센타수리");
        response.setRepairShop(originData.getRepairShop());
        response.setDateRepair(originData.getDateRepair());
        response.setRepairCost(originData.getRepairCost());
        response.setIsCompleted(originData.getIsCompleted() ? "수리완료" : "수리중");
        return response;
    }

    public void putRepairCost(long id, RepairCostChangeRequest request) {
        Repair originData = repairRepository.findById(id).orElseThrow();

        originData.setRepairCost(request.getRepairCost());

        repairRepository.save(originData);
    }

    public void delRepair(long id) {
        repairRepository.deleteById(id);
    }
}
