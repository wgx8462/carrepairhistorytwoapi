package org.gb.carrepairhistorytwoapi.service;

import lombok.RequiredArgsConstructor;
import org.gb.carrepairhistorytwoapi.entity.Car;
import org.gb.carrepairhistorytwoapi.model.car.CarChangeCreateRequest;
import org.gb.carrepairhistorytwoapi.model.car.CarCreateRequest;
import org.gb.carrepairhistorytwoapi.model.car.CarItem;
import org.gb.carrepairhistorytwoapi.model.car.CarResponse;
import org.gb.carrepairhistorytwoapi.repository.CarRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CarService {
    private final CarRepository carRepository;

    public Car getData(long id) {
        return carRepository.findById(id).orElseThrow();
    }

    public void setCar(CarCreateRequest request) {
        Car addData = new Car();
        addData.setCarName(request.getCarName());
        addData.setDateCar(LocalDate.now());
        addData.setEtcMemo(request.getEtcMemo());
        carRepository.save(addData);
    }

    public List<CarItem> getCars() {
        List<Car> originList = carRepository.findAll();
        List<CarItem> result = new LinkedList<>();

        for (Car car : originList) {
            CarItem addItem = new CarItem();
            addItem.setId(car.getId());
            addItem.setCarName(car.getCarName().getName());
            addItem.setDateCar(car.getDateCar());
            addItem.setEtcMemo(car.getEtcMemo());
            result.add(addItem);
        }
        return result;
    }

    public CarResponse getCar(long id) {
        Car originData = carRepository.findById(id).orElseThrow();
        CarResponse response = new CarResponse();

        response.setId(originData.getId());
        response.setCarName(originData.getCarName().getName());
        response.setDateCar(originData.getDateCar());
        response.setEtcMemo(originData.getEtcMemo());
        return response;
    }

    public void putCarChange(long id, CarChangeCreateRequest request) {
        Car originData = carRepository.findById(id).orElseThrow();

        originData.setCarName(request.getCarName());
        originData.setEtcMemo(request.getEtcMemo());
        originData.setDateCar(LocalDate.now());
        carRepository.save(originData);
    }
}
