package org.gb.carrepairhistorytwoapi.repository;

import org.gb.carrepairhistorytwoapi.entity.History;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HistoryRepository extends JpaRepository<History, Long> {
}
