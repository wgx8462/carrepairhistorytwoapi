package org.gb.carrepairhistorytwoapi.repository;

import org.gb.carrepairhistorytwoapi.entity.Car;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<Car, Long> {
}
