package org.gb.carrepairhistorytwoapi.repository;

import org.gb.carrepairhistorytwoapi.entity.Repair;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepairRepository extends JpaRepository<Repair, Long> {
}
