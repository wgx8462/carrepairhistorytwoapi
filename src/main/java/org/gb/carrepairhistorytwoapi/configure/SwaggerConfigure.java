package org.gb.carrepairhistorytwoapi.configure;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
        info = @Info(title = "car_repair_history App",
                description = "car_repair_history_api",
                version = "vi"))
@RequiredArgsConstructor
@Configuration
public class SwaggerConfigure {

    @Bean
    public GroupedOpenApi healthOpenApi() {
        String[] paths = {"/v1/**"};

        return GroupedOpenApi.builder()
                .group("자동차 사고 기록 API v1")
                .pathsToMatch(paths)
                .build();
    }

}