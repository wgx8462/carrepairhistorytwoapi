package org.gb.carrepairhistorytwoapi.controller;

import lombok.RequiredArgsConstructor;
import org.gb.carrepairhistorytwoapi.model.car.CarChangeCreateRequest;
import org.gb.carrepairhistorytwoapi.model.car.CarCreateRequest;
import org.gb.carrepairhistorytwoapi.model.car.CarItem;
import org.gb.carrepairhistorytwoapi.model.car.CarResponse;
import org.gb.carrepairhistorytwoapi.service.CarService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/car")
public class CarController {
    private final CarService carService;

    @PostMapping("/join")
    public String setCar(@RequestBody CarCreateRequest request) {
        carService.setCar(request);

        return "차 등록 완료";
    }

    @GetMapping("/all")
    public List<CarItem> getCars() {
        return carService.getCars();
    }

    @GetMapping("/detail/{id}")
    public CarResponse getCar(@PathVariable long id) {
        return carService.getCar(id);
    }

    @PutMapping("/change/{id}")
    public String putCarChange(@PathVariable long id, @RequestBody CarChangeCreateRequest request) {
        carService.putCarChange(id, request);

        return "수정 완료";
    }
}
