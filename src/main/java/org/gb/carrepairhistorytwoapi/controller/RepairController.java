package org.gb.carrepairhistorytwoapi.controller;

import lombok.RequiredArgsConstructor;
import org.gb.carrepairhistorytwoapi.entity.History;
import org.gb.carrepairhistorytwoapi.model.repair.RepairCostChangeRequest;
import org.gb.carrepairhistorytwoapi.model.repair.RepairCreateRequest;
import org.gb.carrepairhistorytwoapi.model.repair.RepairItem;
import org.gb.carrepairhistorytwoapi.model.repair.RepairResponse;
import org.gb.carrepairhistorytwoapi.service.HistoryService;
import org.gb.carrepairhistorytwoapi.service.RepairService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/repair")
public class RepairController {
    private final HistoryService historyService;
    private final RepairService repairService;

    @PostMapping("/new/history-id/{historyId}")
    public String setRepair(@PathVariable long historyId, @RequestBody RepairCreateRequest request) {
        History history = historyService.getHistoryData(historyId);
        repairService.setRepair(history, request);

        return "등록 완료";
    }

    @GetMapping("/all")
    public List<RepairItem> getRepairs() {
        return repairService.getRepairs();
    }

    @GetMapping("/detail/repair-id/{repairId}")
    public RepairResponse getRepair(@PathVariable long repairId) {
        return repairService.getRepair(repairId);
    }

    @PutMapping("/repair-cost-change/repair-id/{repairId}")
    public String putRepairCost(@PathVariable long repairId, @RequestBody RepairCostChangeRequest request) {
        repairService.putRepairCost(repairId, request);

        return "수정 완료";
    }

    @DeleteMapping("/repair-id/{repairId}")
    public String delRepair(@PathVariable long repairId) {
        repairService.delRepair(repairId);

        return "삭제 완료";
    }
}
