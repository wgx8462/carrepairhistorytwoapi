package org.gb.carrepairhistorytwoapi.controller;

import lombok.RequiredArgsConstructor;
import org.gb.carrepairhistorytwoapi.entity.Car;
import org.gb.carrepairhistorytwoapi.model.history.HistoryCreateRequest;
import org.gb.carrepairhistorytwoapi.model.history.HistoryDateChangeRequest;
import org.gb.carrepairhistorytwoapi.model.history.HistoryItem;
import org.gb.carrepairhistorytwoapi.model.history.HistoryResponse;
import org.gb.carrepairhistorytwoapi.service.CarService;
import org.gb.carrepairhistorytwoapi.service.HistoryService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/history")
public class HistoryController {
    private final CarService carService;
    private final HistoryService historyService;

    @PostMapping("/new/car-id/{carID}")
    public String setHistory(@PathVariable long carID, @RequestBody HistoryCreateRequest request) {
        Car car = carService.getData(carID);
        historyService.setHistory(car, request);

        return "등록 완료";
    }

    @GetMapping("/all")
    public List<HistoryItem> getHistorys() {
        return historyService.getHistorys();
    }

    @GetMapping("/detail/history-id/{historyId}")
    public HistoryResponse getHistory(@PathVariable long historyId) {
        return historyService.getHistory(historyId);
    }

    @PutMapping("/date-change/history-id/{historyId}")
    public String putDateChange(@PathVariable long historyId, @RequestBody HistoryDateChangeRequest request) {
        historyService.putDateChange(historyId, request);

        return "수정 완료";
    }
}
