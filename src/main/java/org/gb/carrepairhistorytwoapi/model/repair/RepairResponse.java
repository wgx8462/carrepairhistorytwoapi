package org.gb.carrepairhistorytwoapi.model.repair;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class RepairResponse {
    private String historyCarCarName;
    private String historyBrokenPart;
    private LocalDate historyDateProblem;
    private String historyIsSelf;
    private String repairShop;
    private LocalDate dateRepair;
    private Double repairCost;
    private String isCompleted;
}
