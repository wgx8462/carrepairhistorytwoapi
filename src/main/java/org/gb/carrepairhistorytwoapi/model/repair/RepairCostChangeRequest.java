package org.gb.carrepairhistorytwoapi.model.repair;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RepairCostChangeRequest {
    private Double repairCost;
}
