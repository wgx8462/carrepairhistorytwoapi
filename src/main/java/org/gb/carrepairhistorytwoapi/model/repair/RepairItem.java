package org.gb.carrepairhistorytwoapi.model.repair;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RepairItem {
    private String historyCarCarName;
    private String historyBrokenPart;
    private String historyIsSelf;
    private String repairShop;
    private Double repairCost;
    private String isCompleted;
}
