package org.gb.carrepairhistorytwoapi.model.repair;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RepairCreateRequest {
    private String repairShop;
    private Double repairCost;
    private Boolean isCompleted;
}
