package org.gb.carrepairhistorytwoapi.model.car;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CarResponse {
    private Long id;
    private String carName;
    private LocalDate dateCar;
    private String etcMemo;
}
