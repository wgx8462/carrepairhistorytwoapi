package org.gb.carrepairhistorytwoapi.model.car;

import lombok.Getter;
import lombok.Setter;
import org.gb.carrepairhistorytwoapi.enums.CarName;

import java.time.LocalDate;

@Getter
@Setter
public class CarItem {
    private Long id;
    private String carName;
    private LocalDate dateCar;
    private String etcMemo;
}
