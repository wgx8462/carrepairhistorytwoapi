package org.gb.carrepairhistorytwoapi.model.car;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;
import org.gb.carrepairhistorytwoapi.enums.CarName;

@Getter
@Setter
public class CarChangeCreateRequest {
    @Enumerated(value = EnumType.STRING)
    private CarName carName;
    private String etcMemo;
}
