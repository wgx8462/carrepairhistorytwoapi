package org.gb.carrepairhistorytwoapi.model.history;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;
import org.gb.carrepairhistorytwoapi.enums.BrokenPart;

@Getter
@Setter
public class HistoryCreateRequest {
    @Enumerated(value = EnumType.STRING)
    private BrokenPart brokenPart;
    private String repairMemo;
    private Boolean isSelf;
}
