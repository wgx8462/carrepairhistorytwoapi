package org.gb.carrepairhistorytwoapi.model.history;

import lombok.Getter;
import lombok.Setter;
import org.gb.carrepairhistorytwoapi.enums.BrokenPart;

@Getter
@Setter
public class HistoryItem {
    private String carCarName;
    private Long id;
    private String brokenPart;
    private String isSelf;
}
