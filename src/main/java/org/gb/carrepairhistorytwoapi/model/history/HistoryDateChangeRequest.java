package org.gb.carrepairhistorytwoapi.model.history;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class HistoryDateChangeRequest {
    private LocalDate dateProblem;
}
