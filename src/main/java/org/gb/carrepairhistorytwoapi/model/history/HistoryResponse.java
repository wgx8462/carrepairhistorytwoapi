package org.gb.carrepairhistorytwoapi.model.history;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class HistoryResponse {
    private String carCarName;
    private Long id;
    private String brokenPart;
    private String isSelf;
    private LocalDate dateProblem;
    private String repairMemo;
}
