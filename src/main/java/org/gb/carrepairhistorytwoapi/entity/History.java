package org.gb.carrepairhistorytwoapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.gb.carrepairhistorytwoapi.enums.BrokenPart;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class History {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carId", nullable = false)
    private Car car;

    @Column(nullable = false)
    private LocalDate dateProblem;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private BrokenPart brokenPart;

    @Column(length = 40)
    private String repairMemo;

    @Column(nullable = false)
    private Boolean isSelf;
}
