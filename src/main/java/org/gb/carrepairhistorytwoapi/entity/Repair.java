package org.gb.carrepairhistorytwoapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Repair {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "historyId", nullable = false)
    private History history;

    @Column(length = 20)
    private String repairShop;

    private LocalDate dateRepair;

    private Double repairCost;

    @Column(nullable = false)
    private Boolean isCompleted;
}
