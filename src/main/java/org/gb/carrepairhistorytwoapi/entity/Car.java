package org.gb.carrepairhistorytwoapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.gb.carrepairhistorytwoapi.enums.CarName;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private CarName carName;

    @Column(nullable = false)
    private LocalDate dateCar;

    private String etcMemo;
}
