package org.gb.carrepairhistorytwoapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarRepairHistoryTwoApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarRepairHistoryTwoApiApplication.class, args);
    }

}
